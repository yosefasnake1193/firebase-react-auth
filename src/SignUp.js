import React, { useCallback } from "react";
import { withRouter } from "react-router";
import app from "./base";

const SignUp = ({ history }) => {
  const handleSignUp = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .createUserWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  return (
    <>
      <h1>SignUp</h1>
      <form onSubmit={handleSignUp}>
        <label>
          Email
          <input name="email" type="email" placeholder="Email" />
        </label>

        <label>
          Password
          <input name="password" type="password" placeholder="Password" />
        </label>

        <label>
          Phone
          <input name="phone" type="number" placeholder="Phone Number" />
        </label>
        <button type="submit">Sign Up</button>
      </form>
    </>
  );
};

export default withRouter (SignUp);
